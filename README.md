Lektor-blog-theme
=======================

A Responsive theme for basic blog module of [lektor](https://www.getlektor.com/) using [bootstrap 4.0-alpha-1](v4-alpha.getbootstrap.com).

[DEMO](http://peeyushsrj.me/lektor-blog-theme/)

![Screenshot](screenshot.png)

Getting started
===============

If you don't have Lektor installed yet, follow the instructions from the official doc [here](https://www.getlektor.com/downloads/).
Instantiate a lektor project. (see [quickstart](https://www.getlektor.com/docs/quickstart/))

Installation
============

Place `style.css`, `bootstrap.min.css` in `assets/static/` directory and `layout.html` in `templates/` directory of your lektor project.

<!-- OR [Download](@) the executable to perform above task. [coming soon]-->